package com.trex.ujwala.entity;

/**
 * Created by administrator on 21/10/16.
 */

public class ELMessage {

    private String MessageID;
    private String Message;
    private String MessageDate;

    public String getMessageID() {
        return MessageID;
    }

    public void setMessageID(String messageID) {
        MessageID = messageID;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getMessageDate() {
        return MessageDate;
    }

    public void setMessageDate(String messageDate) {
        MessageDate = messageDate;
    }
}
