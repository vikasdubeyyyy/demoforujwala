package com.trex.ujwala.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.trex.ujwala.R;
import com.trex.ujwala.entity.ELSpinnerItem;

import java.util.ArrayList;

public class AdapterSpinnerItem extends ArrayAdapter<ELSpinnerItem> {

    private final ArrayList<ELSpinnerItem> list;
    private final Activity context;

    public AdapterSpinnerItem(Activity context, ArrayList<ELSpinnerItem> list) {
        super(context, R.layout.spinneritemrow, list);
        this.context = context;
        this.list = list;
    }

    static class ViewHolder {
        protected TextView OptionTitle;
    }

    public ArrayList<ELSpinnerItem> getSpinnerList() {
        return this.list;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        if (convertView == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            convertView = inflator.inflate(R.layout.spinneritemrow, null);
            viewHolder = new ViewHolder();

            viewHolder.OptionTitle = (TextView) convertView.findViewById(R.id.tvTitle);

            convertView.setTag(viewHolder);
            convertView.setTag(R.id.tvTitle, viewHolder.OptionTitle);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.OptionTitle.setTag(position); // This line is important.
        viewHolder.OptionTitle.setText(list.get(position).getValue());

        return convertView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        if (convertView == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            convertView = inflator.inflate(R.layout.spinneritemrowarrow, null);
            viewHolder = new ViewHolder();

            viewHolder.OptionTitle = (TextView) convertView.findViewById(R.id.tvTitle);

            convertView.setTag(viewHolder);
            convertView.setTag(R.id.tvTitle, viewHolder.OptionTitle);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.OptionTitle.setTag(position); // This line is important.
        viewHolder.OptionTitle.setText(list.get(position).getValue());

        return convertView;
    }


}
