package com.trex.ujwala.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.trex.ujwala.R;
import com.trex.ujwala.entity.ELMessage;

import java.util.List;

public class AdapterMessage extends RecyclerView.Adapter<AdapterMessage.MyViewHolder> {

    private Activity activity;
    private List<ELMessage> list;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvMessageID;
        private TextView tvMessage;
        private TextView tvMessageDate;

        public MyViewHolder(View view) {
            super(view);

            tvMessageID = (TextView) itemView.findViewById(R.id.tvMessageID);
            tvMessage = (TextView) itemView.findViewById(R.id.tvMessage);
            tvMessageDate = (TextView) itemView.findViewById(R.id.tvMessageDate);
        }
    }


    public AdapterMessage(Activity activity, List<ELMessage> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_message, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final ELMessage obj = list.get(position);
        holder.tvMessageID.setText(obj.getMessageID());
        holder.tvMessage.setText(obj.getMessage());
        holder.tvMessageDate.setText(obj.getMessageDate());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}