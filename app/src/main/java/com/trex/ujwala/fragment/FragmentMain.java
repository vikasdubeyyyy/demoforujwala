package com.trex.ujwala.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.trex.ujwala.R;
import com.trex.ujwala.adapter.AdapterMessage;
import com.trex.ujwala.adapter.AdapterSpinnerItem;
import com.trex.ujwala.entity.ELMessage;
import com.trex.ujwala.entity.ELSpinnerItem;

import java.util.ArrayList;


public class FragmentMain extends Fragment {

    private static final String TAG = FragmentMain.class.getSimpleName();

    private Activity activity;
    private Context context;
    private Spinner sMessage;
    private ArrayList<ELSpinnerItem> listMessage = new ArrayList<>();
    private AdapterSpinnerItem adapterMessage;
    private TextView tvOpen;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View layout = inflater.inflate(R.layout.fragment_main, container, false);

        context = getActivity();
        activity = getActivity();
        sMessage = (Spinner) layout.findViewById(R.id.sMessage);
        tvOpen = (TextView)  layout.findViewById(R.id.tvOpen);

        listMessage = getMessageList();
        adapterMessage = new AdapterSpinnerItem(activity, listMessage);
        sMessage.setAdapter(adapterMessage);

        tvOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = new FragmentMessage();

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(R.id.fragment_container, fragment, fragment.getClass().getSimpleName());
                fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
                fragmentTransaction.commit();
            }
        });
        return layout;
    }

    private ArrayList<ELSpinnerItem> getMessageList() {
        ArrayList<ELSpinnerItem> list = new ArrayList<>();

        ELSpinnerItem item = new ELSpinnerItem();
        item.setId("0");
        item.setValue("Select Message");
        list.add(item);

        item = new ELSpinnerItem();
        item.setId("1");
        item.setValue("Other");
        list.add(item);

        item = new ELSpinnerItem();
        item.setId("2");
        item.setValue("Double Entry");
        list.add(item);

        item = new ELSpinnerItem();
        item.setId("3");
        item.setValue("Financial Problem");
        list.add(item);


        return list;
    }


}
