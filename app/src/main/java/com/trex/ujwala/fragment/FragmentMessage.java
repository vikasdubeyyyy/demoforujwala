package com.trex.ujwala.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trex.ujwala.R;
import com.trex.ujwala.adapter.AdapterMessage;
import com.trex.ujwala.entity.ELMessage;

import java.util.ArrayList;


public class FragmentMessage extends Fragment {

    private static final String TAG = FragmentMessage.class.getSimpleName();

    private Activity activity;
    private Context context;
    private RecyclerView rvMessage;
    private LinearLayoutManager mLayoutManager;
    private AdapterMessage adapterMessage;
    private ArrayList<ELMessage> listMessage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View layout = inflater.inflate(R.layout.fragment_message, container, false);

        context = getActivity();
        rvMessage = (RecyclerView) layout.findViewById(R.id.rvMessage);

        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        rvMessage.setLayoutManager(mLayoutManager);
        rvMessage.setItemAnimator(new DefaultItemAnimator());
        rvMessage.setHasFixedSize(true);

        listMessage = getMessageList();
        adapterMessage = new AdapterMessage(activity, listMessage);
        rvMessage.setAdapter(adapterMessage);

        return layout;
    }

    private ArrayList<ELMessage> getMessageList() {
        ArrayList<ELMessage> list = new ArrayList<>();

        ELMessage obj = new ELMessage();
        obj.setMessageID("1");
        obj.setMessage("Hello World");
        obj.setMessageDate("10-05-2015");
        list.add(obj);

        obj = new ELMessage();
        obj.setMessageID("2");
        obj.setMessage("Hello Under World");
        obj.setMessageDate("10-05-2016");
        list.add(obj);

        obj = new ELMessage();
        obj.setMessageID("1");
        obj.setMessage("Hello World War");
        obj.setMessageDate("10-05-2017");
        list.add(obj);

        return list;

    }

}
